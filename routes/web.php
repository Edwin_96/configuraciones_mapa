<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::view('/' , 'Mapa')->name('Mapa');
Route::view('/ReservaBelleza' , 'ReservaBelleza')->name('ReservaBelleza');
Route::view('/Reserva' , 'Reserva')->name('Reserva');
Route::view('/InfoReserva' , 'InfoReserva')->name('InfoReserva');
Route::view('/InfoReservaLugar' , 'InfoReservaLugar')->name('InfoReservaLugar');

Route::view('/pay' , 'pay')->name('pay');
Route::view('/resultsGod' , 'resultsGod')->name('resultsGod');
Route::get('/paypal/pay', 'PaymentController@payWithPayPal');
Route::get('/paypal/status', 'PaymentController@payPalStatus');