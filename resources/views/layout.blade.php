<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <!-- Compiled and minified CSS -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
  <!-- Compiled and minified JavaScript -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>

  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>

  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>

  <link href="https://fonts.googleapis.com/css2?family=Montserrat&display=swap" rel="stylesheet">


  <!-- Core Stylesheet -->

  <link rel="stylesheet" href="evo-calendar/css/evo-calendar.css" />

  <!-- Optional Themes -->

  <link rel="stylesheet" href="evo-calendar/css/evo-calendar.midnight-blue.css" />

  <link rel="stylesheet" href="evo-calendar/css/evo-calendar.orange-coral.css" />

  <link rel="stylesheet" href="evo-calendar/css/evo-calendar.royal-navy.css" />

  <!-- JavaScript -->

  <script src="evo-calendar/js/evo-calendar.js"></script>
  

  <link href="css/style.css" rel="stylesheet" type="text/css">


</head>

<body>
  <header>
    <nav>

      <div class="nav-wrapper">
        <a href="www.youtube.com" class="brand-logo center"><img src="Images/Logo.svg"></a>
        <ul id="nav-mobile" class="left hide-on-med-and-down">
          <li><a href="sass.html"> <i class="material-icons">arrow_back</i></a></li>

        </ul>
      </div>
    </nav>
  </header>
  

  @yield('content')

</body>

</html>