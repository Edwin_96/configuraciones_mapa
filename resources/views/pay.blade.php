@extends('layout')
@section('content')
<br>
<br>

<div class="container">
    <div class="row">
        <div class="col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <a class="waves-effect blue btn pago btn-large" href="{{ url('/paypal/pay') }}">Pagar</a>
        </div>
    </div>
</div>
@endsection