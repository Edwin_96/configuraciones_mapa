@extends('layout')
@section('content')

<div class="container">

    <div class="row">
        <div class="slider col s12">
            <ul class="slides">
                <li>
                    <img src="Images/407712.jpg"> <!-- random image -->
                    <div class="caption center-align">
                        <h3>Prueba</h3>
                        <h5 class="light grey-text text-lighten-3">Prueba</h5>
                    </div>
                </li>
                <li>
                    <img src="Images/2018_BMW_M3_3.0.jpg"> <!-- random image -->
                    <div class="caption left-align">
                        <h3>Y Nelson tambien.</h3>
                        <h5 class="light grey-text text-lighten-3">Prueba.</h5>
                    </div>
                </li>
                <li>
                    <img src="https://lorempixel.com/580/250/nature/3"> <!-- random image -->
                    <div class="caption right-align">
                        <h3>Esto es una prueba</h3>
                        <h5 class="light grey-text text-lighten-3">Esto es una prueba</h5>
                    </div>
                </li>
                <li>
                    <img src="https://lorempixel.com/580/250/nature/4"> <!-- random image -->
                    <div class="caption center-align">
                        <h3>Esto tambien es una prueba</h3>
                        <h5 class="light grey-text text-lighten-3">Esto tambien es una prueba</h5>
                    </div>
                </li>
            </ul>
        </div>

    </div>
    <div class="row">
        <div class="test col s12 m4 l3 ">
            <table class="striped right-align">
                <thead>
                    <tr>
                        <p class="titulo">PRECIO</th>
                    </tr>
                    <tr>
                        <th class="columb">HORA</th>
                        <th class="columb">DIA</th>
                    </tr>
                </thead>
                <tbody class="bod">
                    <tr>
                        <td class="columb">Alvin</td>
                        <td class="columb">Eclair</td>
                    </tr>
                    <tr>
                        <td class="columb">Alan</td>
                        <td class="columb">Jellybean</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="test col s12 m4 l3 offset-l1-5">
            <span class="flow-text">
                <p class="titulo">CAPACIDAD</p>
                <p class="segundo">PERSONAS</p>
                <p class="numero">100</p>
            </span>
        </div>
        <div class="test col s12 m4 l3 offset-l1-5">
            <span class="flow-text">
                <p class="titulo">IBAN</p>
                <p class="segundo">CUENTA</p>
                <p class="numero">10054652546165456</p>
            </span>
        </div>

    </div>
    <br>
    <br>
    <div id="evoCalendar"></div>


    <div class="row float">
        <div class="fixed-action-btn">
            <a class="btn-floating btn-large light blue">
                <i class="large material-icons">list</i>
            </a>
            <ul>
                <li><a class="btn-floating tooltipped green" data-position="left" data-tooltip="Ver condiciones de alquiler"><i class="material-icons">insert_drive_file</i></a>
                </li>
                <li><a class="btn-floating tooltipped red" data-position="left" data-tooltip="Apartar"><i class="material-icons">date_range</i></a></li>

            </ul>
        </div>
    </div>

    <script type='text/javascript' src='JS/carrusel.js'></script>
    <script type='text/javascript' src='JS/calendar.js'></script>


</div>



@endsection