@extends('layout')
@section('content')

<br>
<br>
<div class="container">
    <div class="row">
        <h4 class="title col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">Reserva</h4>
    </div>
    <div class="row">
        <select class="browser-default dropdown col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <option value="" selected>Unico Dia</option>
            <option value="1">Semanal</option>
            <option value="2">Mensual</option>
            <option value="3">Anual</option>
        </select>
    </div>
    <div class="row">
        <p class="col selectInfo s6 m6 xl6 offset-xl3 offset-m3 offset-s3">Seleccione los dias a reservar</p>
    </div>
    <div class="row">
        <div class="col days s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <a class="btn day" id="Lunes" onclick="myFunctionL()">L</a>
            <a class="btn day" id="Martes" onclick="myFunctionK()">K</a>
            <a class="btn day" id="Miercoles" onclick="myFunctionM()">M</a>
            <a class="btn day" id="Jueves" onclick="myFunctionJ()">J</a>
            <a class="btn day" id="Viernes" onclick="myFunctionV()">V</a>
            <a class="btn day" id="Sabado" onclick="myFunctionS()">S</a>
            <a class="btn day" id="Domingo" onclick="myFunctionD()">D</a>

        </div>

    </div>
    <div class="row">
        <div class="input-field col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <i class="material-icons prefix">date_range</i>
            <input id="icon_prefix" type="text" class="datepicker">
            <label for="icon_prefix"> Elige la fecha</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
        <i class="material-icons prefix">alarm</i>
        <input type="text" class="timepicker">
        <label>Hora de inicio</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
        <i class="material-icons prefix">alarm</i>
        <input type="text" class="timepicker">
        <label>Hora a terminar</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <i class="material-icons prefix">create</i>
            <input id="icon_prefix" type="text" class="validate">
            <label>Nombre del tratamiento</label>
        </div>
    </div>
    <div class="row">
        <div class="input-field col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <i class="material-icons prefix">create</i>
            <input id="icon_prefix" type="text" class="validate">
            <label>Descripcion del tratamiento</label>
        </div>
    </div>
    <div class="row">
        <div class="col s6 m6 xl6 offset-xl3 offset-m3 offset-s3">
            <a class="waves-effect blue btn reservar">RESERVAR</a>
        </div>
    </div>

    <script type='text/javascript' src='JS/select.js'></script>
    <script type='text/javascript' src='JS/datepicker.js'></script>

</div>


@endsection